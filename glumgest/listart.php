<?php
include_once "dbcon.php";
$idart = $_GET['idart'];
$datipag = mysql_query("SELECT * FROM operatori WHERE id='$idart'") or die(mysql_error());
while ($pag = mysql_fetch_array($datipag)) {
    $nome = $pag['nome'];
}
?>

<link rel="stylesheet" type="text/css" href="css/backend.css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="library/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" />
<!-- Inclusione librerie js-->
<script language="javascript" type="text/javascript" src="library/jquery.js"></script>
<script language="javascript" type="text/javascript" src="js/i18n/grid.locale-it.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.jqGrid.min.js" ></script>
<script language="javascript" type="text/javascript" src="library/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery-ui-1.8.17.custom.min.js" ></script>
<script language="javascript" type="text/javascript" src="library/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script language="javascript" type="text/javascript" src="library/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script language="javascript" type="text/javascript" src="library/modernizr.js"></script>
<script type="text/javascript" src="js/highlight-active-input.js"></script>
<link rel="stylesheet" type="text/css" href="css/overcast/jquery-ui-1.10.3.custom.css"/>

<script type="text/javascript">

    $(function(){ 
    $("#list").jqGrid({
    url:'reclistartxml.php?idart=<?php echo $idart; ?>',
    datatype: 'xml',
    mtype: 'GET',
    colNames:['Data','Cliente', 'Descrizione'],
    colModel :[ 

    {name:'Data', index:'creation',align:'center',width:'200',ditable:true,sortable:true},
    {name:'Cliente', index:'nominativo', editable:true,width:'200'}, 
    {name:'Descrizione', index:'descrizione',  align:'left',width:'400',editable:true}

    ],
    pager: '#pager',
    rowNum:10,
    rowList:[10,20,30],
    sortname: 'id',
    sortorder: 'asc',
    gridResize:true,

    viewrecords: true,
 autowidth:'yes',
    height:'auto',
    gridview: true,
    caption: 'Diario di <?php echo $nome; ?>',
    editurl:"listart.php?idart=id"
    }); 

    jQuery("#list").dblclick( function() {
    var s;
    s = jQuery("#list").jqGrid('getGridParam','selrow');
   
    s = jQuery("#list").jqGrid('getGridParam','selrow');
    var dest= "detpaper.php&idpaper="+s;
    window.location = "index.php?p="+dest; 
    $.fancybox({

   
    'width'		        : '800',
    'height'			: '98%',
    'autoScale'			: true,
    'transitionIn'		: 'none',
    'transitionOut'		: 'none',
    'hideOnOverlayClick': false,
    'type'				: 'iframe'
    });
  


    });



    jQuery("#list").jqGrid('navGrid','#pager',{add:false,del:false,edit:false,position:'right'});
    jQuery("#m1").click( function() {
    var s;
    s = jQuery("#list").jqGrid('getGridParam','selrow');
    if( s != null ){
    s = jQuery("#list").jqGrid('getGridParam','selrow');
    var dest= "editart.php?idart="+s;
window.location = "index.php?p="+dest;
    $.fancybox({

   
    'width'				: '90%',
    'height'			: '90%',
    'autoScale'			: true,
    'transitionIn'		: 'none',
    'transitionOut'		: 'none',
    'hideOnOverlayClick': false,
    'type'				: 'iframe',
    'onClosed': function() {
    parent.location.reload(true);
    }
    }); 
    }else {alert('Selezionare una riga');
    window.location = "index.php";  }


    });
    jQuery("#m2").click( function() {
    var s; s = jQuery("#list").jqGrid('getGridParam','selrow');
    if( s != null ){
    s = jQuery("#list").jqGrid('getGridParam','selrow');
    doIt=confirm("Attenzione! Si sta per eliminare la pagina! Proseguire con la cancellazione?");
    if(doIt){
    window.location = "delart.php?idart="+s; }else
    {
    window.location='index.php'; 
    }}else{
    alert('Selezionare una riga');
    }

    });
    jQuery("#m3").click( function() {
    var s; s = jQuery("#list").jqGrid('getGridParam','selrow');
    if( s != null ){
    s = jQuery("#list").jqGrid('getGridParam','selrow');    
    window.open("../index.php?p="+s,"_blank"); 
    }else {alert('Selezionare una riga');
    }

    }
    );
    }); 


    $(document).ready(function() {
    $("#new").fancybox({
    'width'				: '90%',
    'height'			: '90%',
    'autoScale'			: true,
    'transitionIn'		: 'none',
    'transitionOut'		: 'none',
    'hideOnOverlayClick': false,
    'type'				: 'iframe',
    'onClosed': function() {
    parent.location.reload(true);
    }
    });


    });

</script>
<div style="text-align: center; width: 100%;
     ">
    <div id="testolist">
        <font style="font-size: 16px;">Di seguito la lista dei lavori di: 
            <b><?php echo $nome; ?></b>
        </font>
    </div>
    <br/>
    <div style="text-align: center; background-color: red; width: 100%;">
    <table id="list" id="boxcentrato"></table>
    <div id="pager"></div>
    </div>
</div>