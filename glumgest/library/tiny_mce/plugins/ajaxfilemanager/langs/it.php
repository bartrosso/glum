<?php
	/**
	 * language pack
	 * @author Logan Cai (cailongqun [at] yahoo [dot] com [dot] cn)
         * italian translate Francesco Tangalli bartrosso@gmail.com :-)
	 * @link www.phpletter.com
	 * @since 22/April/2007
	 *
	 */
	define('DATE_TIME_FORMAT', 'd/M/Y H:i:s');
	//Common
	//Menu
	
	
	
	
	define('MENU_SELECT', 'Carica selezionati');
	define('MENU_DOWNLOAD', 'Scarica');
	define('MENU_PREVIEW', 'Anteprima');
	define('MENU_RENAME', 'Rinomina');
	define('MENU_EDIT', 'modifica');
	define('MENU_CUT', 'Taglia');
	define('MENU_COPY', 'Copia');
	define('MENU_DELETE', 'Cancella');
	define('MENU_PLAY', 'Play');
	define('MENU_PASTE', 'Incolla');
	
	//Label
		//Top Action
		define('LBL_ACTION_REFRESH', 'Aggiorna');
		define('LBL_ACTION_DELETE', 'Cancella');
		define('LBL_ACTION_CUT', 'Taglia');
		define('LBL_ACTION_COPY', 'Copia');
		define('LBL_ACTION_PASTE', 'Incolla');
		define('LBL_ACTION_CLOSE', 'Chiudi');
		define('LBL_ACTION_SELECT_ALL', 'Seleziona tutto');
		//File Listing
	define('LBL_NAME', 'Nome');
	define('LBL_SIZE', 'Dimensione');
	define('LBL_MODIFIED', 'Modificato il');
		//File Information
	define('LBL_FILE_INFO', 'Informazioni Immagini:');
	define('LBL_FILE_NAME', 'Nome:');	
	define('LBL_FILE_CREATED', 'Creato:');
	define('LBL_FILE_MODIFIED', 'Modificato:');
	define('LBL_FILE_SIZE', 'File Size:');
	define('LBL_FILE_TYPE', 'File Type:');
	define('LBL_FILE_WRITABLE', 'Scrivibile?');
	define('LBL_FILE_READABLE', 'Leggibile?');
		//Folder Information
	define('LBL_FOLDER_INFO', 'Informazioni cartella');
	define('LBL_FOLDER_PATH', 'Cartella:');
	define('LBL_CURRENT_FOLDER_PATH', 'Indirizzo Cartella attuale:');
	define('LBL_FOLDER_CREATED', 'Creata:');
	define('LBL_FOLDER_MODIFIED', 'Modificata:');
	define('LBL_FOLDER_SUDDIR', 'Sottocartelle:');
	define('LBL_FOLDER_FIELS', 'Files:');
	define('LBL_FOLDER_WRITABLE', 'Scrivibile?');
	define('LBL_FOLDER_READABLE', 'Leggibile?');
	define('LBL_FOLDER_ROOT', 'Cartella Radice');
		//Preview
	define('LBL_PREVIEW', 'Anteprima');
	define('LBL_CLICK_PREVIEW', 'Clicca per vedere Anteprima.');
	//Buttons
	define('LBL_BTN_SELECT', 'Seleziona');
	define('LBL_BTN_CANCEL', 'Cancella');
	define('LBL_BTN_UPLOAD', 'Carica');
	define('LBL_BTN_CREATE', 'Crea');
	define('LBL_BTN_CLOSE', 'Chiudi');
	define('LBL_BTN_NEW_FOLDER', 'Nuova Cartella');
	define('LBL_BTN_NEW_FILE', 'Nuovo File');
	define('LBL_BTN_EDIT_IMAGE', 'Modifica');
	define('LBL_BTN_VIEW', 'Seleziona vista');
	define('LBL_BTN_VIEW_TEXT', 'Testo');
	define('LBL_BTN_VIEW_DETAILS', 'Dettagli');
	define('LBL_BTN_VIEW_THUMBNAIL', 'Thumbnails');
	define('LBL_BTN_VIEW_OPTIONS', 'Vedi In:');
	//pagination
	define('PAGINATION_NEXT', 'Prossimo');
	define('PAGINATION_PREVIOUS', 'Precedente');
	define('PAGINATION_LAST', 'Ultimo');
	define('PAGINATION_FIRST', 'Primo');
	define('PAGINATION_ITEMS_PER_PAGE', 'Mostra %s oggetti per pagina');
	define('PAGINATION_GO_PARENT', 'Vai alla Cartella superiore');
	//System
	define('SYS_DISABLED', 'Permesso negato: Il sistema è disabilitato.');
	
	//Cut
	define('ERR_NOT_DOC_SELECTED_FOR_CUT', 'Nessuna Cartella selezionata per il taglio.');
	//Copy
	define('ERR_NOT_DOC_SELECTED_FOR_COPY', 'Nessuna Cartella selezionata per la copia.');
	//Paste
	define('ERR_NOT_DOC_SELECTED_FOR_PASTE', 'Nessuna Cartella selezionata da incollare.');
	define('WARNING_CUT_PASTE', 'Sei sicuro di voler incollare i documenti in questa cartella?');
	define('WARNING_COPY_PASTE', 'Sei sicuro di voler copiare i documenti in questa cartella?');
	define('ERR_NOT_DEST_FOLDER_SPECIFIED', 'Nessuna cartella di destinazione specificata.');
	define('ERR_DEST_FOLDER_NOT_FOUND', 'Cartella di destinazione non trovata.');
	define('ERR_DEST_FOLDER_NOT_ALLOWED', 'Non sei autorizzato a copiare i file qui');
	define('ERR_UNABLE_TO_MOVE_TO_SAME_DEST', 'Errore nel copiare il file (%s): Origine è uguale a destinazione.');
	define('ERR_UNABLE_TO_MOVE_NOT_FOUND', 'Errore nel copiare il file (%s): File origine non esistente.');
	define('ERR_UNABLE_TO_MOVE_NOT_ALLOWED', 'Errore nel copiare il file (%s): File originale con accesso negato.');
 
	define('ERR_NOT_FILES_PASTED', 'Nessun file è stato copiato.');

	//Search
	define('LBL_SEARCH', 'Cerca');
	define('LBL_SEARCH_NAME', 'Nome del file completo/parziale:');
	define('LBL_SEARCH_FOLDER', 'Guarda in:');
	define('LBL_SEARCH_QUICK', 'Ricerca Veloce');
	define('LBL_SEARCH_MTIME', 'modificato il:  Time(Range):');
	define('LBL_SEARCH_SIZE', 'Dimensione File:');
	define('LBL_SEARCH_ADV_OPTIONS', 'Opzioni avanzate');
	define('LBL_SEARCH_FILE_TYPES', 'Tipi di file:');
	define('SEARCH_TYPE_EXE', 'Applicazioni');
	
	define('SEARCH_TYPE_IMG', 'Immagine');
	define('SEARCH_TYPE_ARCHIVE', 'Archivio');
	define('SEARCH_TYPE_HTML', 'HTML');
	define('SEARCH_TYPE_VIDEO', 'Video');
	define('SEARCH_TYPE_MOVIE', 'Movie');
	define('SEARCH_TYPE_MUSIC', 'Musica');
	define('SEARCH_TYPE_FLASH', 'Flash');
	define('SEARCH_TYPE_PPT', 'PowerPoint');
	define('SEARCH_TYPE_DOC', 'Documento');
	define('SEARCH_TYPE_WORD', 'Word');
	define('SEARCH_TYPE_PDF', 'PDF');
	define('SEARCH_TYPE_EXCEL', 'Excel');
	define('SEARCH_TYPE_TEXT', 'Text');
	define('SEARCH_TYPE_UNKNOWN', 'Sconosciuto');
	define('SEARCH_TYPE_XML', 'XML');
	define('SEARCH_ALL_FILE_TYPES', 'Tutti i tipi');
	define('LBL_SEARCH_RECURSIVELY', 'Ricerca ricorsiva:');
	define('LBL_RECURSIVELY_YES', 'Si');
	define('LBL_RECURSIVELY_NO', 'No');
	define('BTN_SEARCH', 'Cerca ora');
	//thickbox
	define('THICKBOX_NEXT', 'Prossima&gt;');
	define('THICKBOX_PREVIOUS', '&lt;Precedente');
	define('THICKBOX_CLOSE', 'Chiudi');
	//Calendar
	define('CALENDAR_CLOSE', 'Chiudi');
	define('CALENDAR_CLEAR', 'Pulisci');
	define('CALENDAR_PREVIOUS', '&lt;Precedente');
	define('CALENDAR_NEXT', 'Prossima&gt;');
	define('CALENDAR_CURRENT', 'Oggi');
	define('CALENDAR_MON', 'Lun');
	define('CALENDAR_TUE', 'Mar');
	define('CALENDAR_WED', 'Mer');
	define('CALENDAR_THU', 'Gio');
	define('CALENDAR_FRI', 'Ven');
	define('CALENDAR_SAT', 'Sab');
	define('CALENDAR_SUN', 'Dom');
	define('CALENDAR_JAN', 'Gen');
	define('CALENDAR_FEB', 'Feb');
	define('CALENDAR_MAR', 'Mar');
	define('CALENDAR_APR', 'Apr');
	define('CALENDAR_MAY', 'Mag');
	define('CALENDAR_JUN', 'giu');
	define('CALENDAR_JUL', 'Lug');
	define('CALENDAR_AUG', 'Ago');
	define('CALENDAR_SEP', 'Set');
	define('CALENDAR_OCT', 'Ott');
	define('CALENDAR_NOV', 'Nov');
	define('CALENDAR_DEC', 'Dic');
	//ERROR MESSAGES
		//deletion
	define('ERR_NOT_FILE_SELECTED', 'Per favore selezionare un file.');
	define('ERR_NOT_DOC_SELECTED', 'Nessun documento selezionato per l eleminazione.');
	define('ERR_DELTED_FAILED', 'Impossivile cancellare i documenti.');
	define('ERR_FOLDER_PATH_NOT_ALLOWED', 'Indirizzo non amesso.');
		//class manager
	define('ERR_FOLDER_NOT_FOUND', 'Impossibile trovare l indirizzo specificato: ');
		//rename
	define('ERR_RENAME_FORMAT', 'Per favore dare un nome che contenga solo lettere, cifre, spazi, trattini e barre basse.');
	define('ERR_RENAME_EXISTS', 'Per favore dare un nome che sia univoco dentro questa cartella.');
	define('ERR_RENAME_FILE_NOT_EXISTS', 'Il file/cartella non esiste.');
	define('ERR_RENAME_FAILED', 'Impossibile rinominare, per favore riprovare.');
	define('ERR_RENAME_EMPTY', 'Per favore dare un nome');
	define('ERR_NO_CHANGES_MADE', 'Nessun cambiamento effettuato.');
	define('ERR_RENAME_FILE_TYPE_NOT_PERMITED', 'Non sei autorizzato a cambiare file con questa estensione.');
		//folder creation
	define('ERR_FOLDER_FORMAT', 'Per favore dare un nome che contenga solo lettere, cifre, spazi, trattini e barre basse.');
	define('ERR_FOLDER_EXISTS', 'Per favore dare un nome che sia univoco dentro questa cartella.');
	define('ERR_FOLDER_CREATION_FAILED', 'Impossibile creare la cartella, riprovare');
	define('ERR_FOLDER_NAME_EMPTY', 'Per favore dare un nome');
	define('FOLDER_FORM_TITLE', 'Form per nuova cartella');
	define('FOLDER_LBL_TITLE', 'Titolo cartella:');
	define('FOLDER_LBL_CREATE', 'Crea cartella');
	//New File
	define('NEW_FILE_FORM_TITLE', 'Form per nuovo file');
	define('NEW_FILE_LBL_TITLE', 'Nome file:');
	define('NEW_FILE_CREATE', 'Crea file');
		//file upload
	define('ERR_FILE_NAME_FORMAT', 'Per favore dare un nome che contenga solo lettere, cifre, spazi, trattini e barre basse.');
	define('ERR_FILE_NOT_UPLOADED', 'Nessun file selezionato per il caricamento');
	define('ERR_FILE_TYPE_NOT_ALLOWED', 'Non sei autorizzato a caricare questo tipo di file.');
	define('ERR_FILE_MOVE_FAILED', 'Impossibile spostare il file');
	define('ERR_FILE_NOT_AVAILABLE', 'Il file non si trova');
	define('ERROR_FILE_TOO_BID', 'File troppo grande. (max: %s)');
	define('FILE_FORM_TITLE', 'Carica File');
	define('FILE_LABEL_SELECT', 'Seleziona File');
	define('FILE_LBL_MORE', 'Aggiungi File da caricare');
	define('FILE_CANCEL_UPLOAD', 'Elimina File');
	define('FILE_LBL_UPLOAD', 'Carica');
	//file download
	define('ERR_DOWNLOAD_FILE_NOT_FOUND', 'Nessun file selezionato per il caricamento.');
	//Rename
	define('RENAME_FORM_TITLE', 'Rinomina');
	define('RENAME_NEW_NAME', 'Nuovo Nome');
	define('RENAME_LBL_RENAME', 'Rinomina');

	//Tips
	define('TIP_FOLDER_GO_DOWN', 'Fare Click per prendere la cartella...');
	define('TIP_DOC_RENAME', 'Fare Doppio Click per modificare...');
	define('TIP_FOLDER_GO_UP', 'Fare Click per nadare alla cartella superiore...');
	define('TIP_SELECT_ALL', 'Seleziona tutto');
	define('TIP_UNSELECT_ALL', 'deseleziona tutto');
	//WARNING
	define('WARNING_DELETE', 'Sei sicuro di voler eliminare i documenti selezionati?');
	define('WARNING_IMAGE_EDIT', 'Seleziona un immagine da modificare');
	define('WARNING_NOT_FILE_EDIT', 'Seleziona un documento da modificare');
	define('WARING_WINDOW_CLOSE', 'Sei sicuro di chiudere la finestra?');
	//Preview
	define('PREVIEW_NOT_PREVIEW', 'Nessuna anteprima disponibile.');
	define('PREVIEW_OPEN_FAILED', 'Impossibile aprire il file.');
	define('PREVIEW_IMAGE_LOAD_FAILED', 'impossibile caricare l immagine');

	//Login
	define('LOGIN_PAGE_TITLE', 'Ajax File Manager Login ');
	define('LOGIN_FORM_TITLE', 'Login ');
	define('LOGIN_USERNAME', 'Username:');
	define('LOGIN_PASSWORD', 'Password:');
	define('LOGIN_FAILED', 'Username/password non validi.');
	
	
	//88888888888   Below for Image Editor   888888888888888888888
		//Warning 
		define('IMG_WARNING_NO_CHANGE_BEFORE_SAVE', 'Non hai fatto alcun cambiamento alla immagine.');
		
		//General
		define('IMG_GEN_IMG_NOT_EXISTS', 'Immagine non esistente');
		define('IMG_WARNING_LOST_CHANAGES', 'Tutti i cambiamenti non salvati andranno persi,sei sicuro di voler continuare?');
		define('IMG_WARNING_REST', 'Tutti i cambiamenti non salvati andranno persi,sei sicuro di voler resettare?');
		define('IMG_WARNING_EMPTY_RESET', 'Nessun cambiamento è stato fatto finora');
		define('IMG_WARING_WIN_CLOSE', 'Sei sicuro di chiudere la finestra?');
		define('IMG_WARNING_UNDO', 'Sei sicuro di ripristinare l immagine precedente?');
		define('IMG_WARING_FLIP_H', 'Sei sicuro di voler ruotare l immagine orizzontalmente?');
		define('IMG_WARING_FLIP_V', 'Sei sicuro di voler ruotare l immagine verticalmente');
		define('IMG_INFO', 'Informazioni sull immagine ');
		
		//Mode
			define('IMG_MODE_RESIZE', 'Ridimensiona:');
			define('IMG_MODE_CROP', 'Taglia:');
			define('IMG_MODE_ROTATE', 'Ruota:');
			define('IMG_MODE_FLIP', 'Ribalta:');		
		//Button
		
			define('IMG_BTN_ROTATE_LEFT', '90&deg;CCW');
			define('IMG_BTN_ROTATE_RIGHT', '90&deg;CW');
			define('IMG_BTN_FLIP_H', 'Ribalta Orizzontale');
			define('IMG_BTN_FLIP_V', 'Ribalta Verticale');
			define('IMG_BTN_RESET', 'Reset');
			define('IMG_BTN_UNDO', 'Annulla');
			define('IMG_BTN_SAVE', 'Salva');
			define('IMG_BTN_CLOSE', 'Chiudi');
			define('IMG_BTN_SAVE_AS', 'Salva come..');
			define('IMG_BTN_CANCEL', 'Cancella');
		//Checkbox
			define('IMG_CHECKBOX_CONSTRAINT', 'Riduci?');
		//Label
			define('IMG_LBL_WIDTH', 'Larghezza:');
			define('IMG_LBL_HEIGHT', 'Altezza:');
			define('IMG_LBL_X', 'X:');
			define('IMG_LBL_Y', 'Y:');
			define('IMG_LBL_RATIO', 'Rapporto:');
			define('IMG_LBL_ANGLE', 'Angolo:');
			define('IMG_LBL_NEW_NAME', 'Nuovo nome:');
			define('IMG_LBL_SAVE_AS', 'Salva come Form');
			define('IMG_LBL_SAVE_TO', 'Salva in:');
			define('IMG_LBL_ROOT_FOLDER', 'Cartella principale');
		//Editor
		//Save as 
		define('IMG_NEW_NAME_COMMENTS', 'Per favore non inserire l estensione dell immagine');
		define('IMG_SAVE_AS_ERR_NAME_INVALID', 'Per favore dare un nome che contenga solo lettere, cifre, spazi, trattini e barre basse.');
		define('IMG_SAVE_AS_NOT_FOLDER_SELECTED', 'Nessuna cartella di destinazione selezionata');	
		define('IMG_SAVE_AS_FOLDER_NOT_FOUND', 'TLa cartella di destinazione non esiste');
		define('IMG_SAVE_AS_NEW_IMAGE_EXISTS', 'Ci sono immagini con lo stesso nome.');

		//Save
		define('IMG_SAVE_EMPTY_PATH', 'Path dell immagine vuota.');
		define('IMG_SAVE_NOT_EXISTS', 'Immagine non esistente.');
		define('IMG_SAVE_PATH_DISALLOWED', 'Non sei autorizzato ad accedere al  file.');
		define('IMG_SAVE_UNKNOWN_MODE', 'Operation Mode inaspettato sull immagine');
		define('IMG_SAVE_RESIZE_FAILED', 'Errore nel ridimensionare l immagine');
		define('IMG_SAVE_CROP_FAILED', 'Errore nel tgliare l immagine');
		define('IMG_SAVE_FAILED', 'Errore nel salvare l immagine');
		define('IMG_SAVE_BACKUP_FAILED', 'Impossibile tornare indietro .');
		define('IMG_SAVE_ROTATE_FAILED', 'Impossibile ruotare l immagine');
		define('IMG_SAVE_FLIP_FAILED', 'Impossibile ribaltare l immagine.');
		define('IMG_SAVE_SESSION_IMG_OPEN_FAILED', 'Impossibile aprire l immagine da sessione.');
		define('IMG_SAVE_IMG_OPEN_FAILED', 'Impossibile aprire l immagine');
		
		
		//UNDO
		define('IMG_UNDO_NO_HISTORY_AVAIALBE', 'Nessuna cronologia per annullare.');
		define('IMG_UNDO_COPY_FAILED', 'Impossibile ripristinare l immagine.');
		define('IMG_UNDO_DEL_FAILED', 'Impossibile cancellare l immagine');
	
	//88888888888   Above for Image Editor   888888888888888888888
	
	//88888888888   Session   888888888888888888888
		define('SESSION_PERSONAL_DIR_NOT_FOUND', 'Impossibile trovare la cartella dedicata che sarebbe dovuta essere creata ');
		define('SESSION_COUNTER_FILE_CREATE_FAILED', 'Impossibile aprire il file contatore di sessione');
		define('SESSION_COUNTER_FILE_WRITE_FAILED', 'Impossibile scrivere il file contatore di sessione.');
	//88888888888   Session   888888888888888888888
	
	//88888888888   Below for Text Editor   888888888888888888888
		define('TXT_FILE_NOT_FOUND', 'File non trovato.');
		define('TXT_EXT_NOT_SELECTED', 'Per favore seleziona l estensione del file');
		define('TXT_DEST_FOLDER_NOT_SELECTED', 'Per favore selezionare cartella di destinazione');
		define('TXT_UNKNOWN_REQUEST', 'Richiesta sconosciuta.');
		define('TXT_DISALLOWED_EXT', 'non sei autorizzato a modificare/creare questo tipo di file.');
		define('TXT_FILE_EXIST', 'Questo file esiste già.');
		define('TXT_FILE_NOT_EXIST', 'nessun risultato.');
		define('TXT_CREATE_FAILED', 'Errore nel creare nuovo file.');
		define('TXT_CONTENT_WRITE_FAILED', 'Errore nello scrivere contenuto nel file.');
		define('TXT_FILE_OPEN_FAILED', 'Errore nell aprire il file.');
		define('TXT_CONTENT_UPDATE_FAILED', 'Errore nell aggiornare il file.');
		define('TXT_SAVE_AS_ERR_NAME_INVALID', 'Per favore dare un nome che contenga solo lettere, cifre, spazi, trattini e barre basse.');
	//88888888888   Above for Text Editor   888888888888888888888
	
	
?>