<script type="text/javascript">
    
    $(function(){ 
        $("#list").jqGrid({
            url:'recclientixml.php',
            datatype: 'xml',
            mtype: 'GET',
            colNames:['Nominativo'],
            colModel :[ 
               
                {name:'nominativo',width:600, index:'nominativo',editable:true,sortable:true}
                
            ],
            pager: '#pager',
            rowNum:10,
            rowList:[10,20,30],
            sortname: 'id',
            sortorder: 'asc',
            gridResize:true,
            
            forceFit:true,
            viewrecords: true,
            height:'auto',
            gridview: true,
            caption: 'Operatori',
            editurl:"editart.php?idart=id"
        }); 

jQuery("#list").dblclick( function() {
            var s;
            s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');
                var dest= "editcliente.php?idcliente="+s;

                $.fancybox({

                    'href' : dest,
                    'title':'Modifica Cliente',
                    'transitionIn'		: 'none',
                    'transitionOut'		: 'none',
                    'hideOnOverlayClick': false,
                    'type'				: 'iframe',
                    'onClosed': function() {
                        parent.location.reload(true);
                    }
                });
            }else {alert('Selezionare una riga');
                window.location = "index.php";  }


        });


  
        jQuery("#list").jqGrid('navGrid','#pager',{add:false,del:false,edit:false,position:'right'});
        jQuery("#m1").click( function() {
            var s;
            s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');
                var dest= "editcliente.php?idcliente="+s;
          
                $.fancybox({
                
                    'href' : dest,
                 
      
                    'transitionIn'		: 'none',
                    'transitionOut'		: 'none',
                    'title':'Modifica Cliente',
                    'hideOnOverlayClick': false,
                    'type'				: 'iframe',
                    'onClosed': function() {
                        parent.location.reload(true);
                    }
                }); 
            }else {alert('Selezionare una riga');
                window.location = "index.php";  }
     
    
        });
        jQuery("#m2").click( function() {
            var s; s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');
              doIt=confirm("Attenzione! Si sta per eliminare la pagina! Proseguire con la cancellazione?");
 if(doIt){
            window.location = "delcliente.php?idcliente="+s; }else
            {
                window.location='index.php'; 
            }}else{
            alert('Selezionare una riga');
            }
    
        });
           jQuery("#m3").click( function() {
                           var s; s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');    
                    window.open("../index.php?p="+s,"_blank"); 
            }else {alert('Selezionare una riga');
               }
              
           }
       );
    }); 


    $(document).ready(function() {
        $("#new").fancybox({
            'width'			: '90%',
            'height'			: '90%',
            'autoScale'			: true,
            'transitionIn'		: 'none',
            'transitionOut'		: 'none',
            'hideOnOverlayClick': false,
            'type'				: 'iframe',
            'onClosed': function() {
                parent.location.reload(true);
            }
        });


    });

    function newCliente(){
       var r=prompt('Nuovo cliente', 'Inserisci il nuovo nominativo');

            location.href=('newcliente2.php?namecliente='+r);
    }
    
    
</script>
<div id="pulsantiera">
<button id="new" onclick="newCliente()">
    <img src="images/plus_16.png">  
  <span class="textbutton">  Nuovo</span>
</button>
<button class="editbutton" href="javascript:void(0)" id="m1">
    <img src="images/pencil_16.png"> 
    <span class="textbutton">   Modifica</span>
</button>
<button class="delbutton" href="javascript:void(0)" id="m2">
    <img src="images/delete_16.png">
    <span class="textbutton">   Elimina</span>
</button>

    
</div>
<table id="list"></table>
<div id="pager"></div>
